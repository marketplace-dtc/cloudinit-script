#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

MYSQL_ROOT_PASS='password'
MYSQL_USER_NAME='dbuser'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='db_owncloud'
ADMIN_USER='admin'
ADMIN_PASSWORD='password!@#'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/owncloud_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--db-root-password=ROOTPASSWORD  
--db-user-name=USERNAME 
--db-user-password=USERPASSWORD 
--db-database-name=DATABASENAME 
--admin-user=useradmin 
--admin-password=password 
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "OwnCLoud_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--db-root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--db-user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--db-user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--db-database-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-database-name=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--admin-user)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_USER=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-user=?*)
					ADMIN_USER=${1#*=}
					;;
		--admin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-password=?*)
					ADMIN_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
				
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
install_dir="/var/www/html"
#Creating OwnCLoud Database Credenitals
db_name=$MYSQL_USER_DB
db_user=$MYSQL_USER_NAME
db_password=$MYSQL_USER_PASS
mysqlrootpass=$MYSQL_ROOT_PASS

#### Install Packages for https and mysql
apt -y update 
apt -y upgrade
apt -y install lynx
apt -y install apache2
apt -y install mysql-server
apt -y install php php-opcache php-gd php-curl php-mysqlnd php-intl php-json php-ldap php-mbstring php-mysqlnd php-xml php-zip
apt -y install letsencrypt
apt -y install python3-certbot-apache

#### Start http
rm /var/www/html/index.html
systemctl enable apache2
systemctl start apache2

#### Start mysql and set root password

systemctl enable mysql
systemctl start mysql

/usr/bin/mysql -e "USE mysql;"
/usr/bin/mysql -e "UPDATE user SET Password=PASSWORD($mysqlrootpass) WHERE user='root';"
/usr/bin/mysql -e "FLUSH PRIVILEGES;"
touch /root/.my.cnf
chmod 640 /root/.my.cnf
echo "[client]">>/root/.my.cnf
echo "user=root">>/root/.my.cnf
echo "password="$mysqlrootpass>>/root/.my.cnf
####Install PHP
apt -y install php php-bz2 php-mysqli php-curl php-gd php-intl php-common php-mbstring php-xml

sed -i '0,/AllowOverride\ None/! {0,/AllowOverride\ None/ s/AllowOverride\ None/AllowOverride\ All/}' /etc/apache2/apache2.conf #Allow htaccess usage

systemctl restart apache2

####Download and extract latest OwnCloud packages
if test -f /tmp/owncloud-complete-latest.tar.bz2
then
echo "OwnCloud is already downloaded."
else
echo "Downloading OwnCloud"
cd /tmp/ && wget "https://download.owncloud.com/server/stable/owncloud-complete-latest.tar.bz2";
fi

/bin/tar -C $install_dir -xvf /tmp/owncloud-complete-latest.tar.bz2 --strip-components=1
chown www-data: $install_dir -R



##### Set Mysql Access
/usr/bin/mysql -u root -e "CREATE DATABASE $db_name"
/usr/bin/mysql -u root -e "CREATE USER '$db_user'@'localhost' IDENTIFIED WITH mysql_native_password BY '$db_password';"
/usr/bin/mysql -u root -e "GRANT ALL PRIVILEGES ON $db_name.* TO '$db_user'@'localhost';"

#### Init OwnCLoud Config and set DB credentials
  curl --location --request POST 'http://127.0.0.1/index.php' \
    --header 'Connection: keep-alive' \
    --header 'Cache-Control: max-age=0' \
    --header 'Upgrade-Insecure-Requests: 1' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
    --data-urlencode 'install=true' \
    --data-urlencode 'adminlogin='$ADMIN_USER \
    --data-urlencode 'adminpass='$ADMIN_PASSWORD \
    --data-urlencode 'adminpass-clone='$ADMIN_PASSWORD \
    --data-urlencode 'showadminpass=on' \
    --data-urlencode 'directory=/var/www/html/data' \
    --data-urlencode 'dbtype=mysql' \
    --data-urlencode 'dbuser='$db_user \
    --data-urlencode 'dbpass='$db_password \
    --data-urlencode 'dbpass-clone='$db_password \
    --data-urlencode 'dbname='$db_name \
    --data-urlencode 'dbhost=localhost'

sleep 5m
/bin/sed -i "s/'127.0.0.1'/\$_SERVER['HTTP_HOST']/g" $install_dir/config/config.php
chown www-data: $install_dir -R

######Display generated passwords to log file.
echo "Database Name: " $db_name
echo "Database User: " $db_user
echo "Database Password: " $db_password
echo "Mysql root password: " $mysqlrootpass
echo "admin user: " $ADMIN_USER
echo "admin password: " $ADMIN_PASSWORD

echo '=> OwnCloud setup done'


curl --location --request GET "$CALLBACKURL"