#!/usr/bin/env bash
sudo apt-get update
sudo apt-get upgrade -y

curl -fsSL https://get.docker.com | bash
DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
mkdir -p $DOCKER_CONFIG/cli-plugins
curl -SL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose
git clone https://github.com/frappe/frappe_docker
cd frappe_docker
mkdir ~/gitops
echo 'TRAEFIK_DOMAIN=traefikdev.cloudciti.io' > ~/gitops/traefik.env
echo 'EMAIL=admin@example.com' >> ~/gitops/traefik.env
echo 'HASHED_PASSWORD='$(openssl passwd -apr1 changeit | sed 's/\$/\\\$/g') >> ~/gitops/traefik.env
echo 'USERNAME=admin' >> ~/gitops/traefik.env
docker compose --project-name traefik  --env-file ~/gitops/traefik.env  -f docs/compose/compose.traefik.yaml  -f docs/compose/compose.traefik-ssl.yaml up -d
echo "DB_PASSWORD=changeit" > ~/gitops/mariadb.env
docker compose --project-name mariadb --env-file ~/gitops/mariadb.env -f docs/compose/compose.mariadb-shared.yaml up -d
echo '#' >> ~/gitops/erpnext-one.env
cp example.env ~/gitops/erpnext-one.env
sed -i 's/DB_PASSWORD=123/DB_PASSWORD=changeit/g' ~/gitops/erpnext-one.env
sed -i 's/DB_HOST=/DB_HOST=mariadb-database/g' ~/gitops/erpnext-one.env
sed -i 's/DB_PORT=/DB_PORT=3306/g' ~/gitops/erpnext-one.env
echo 'ROUTER=erpnext-one' >> ~/gitops/erpnext-one.env
echo "SITES=\`erpdev.dcloud.co.id\`" >> ~/gitops/erpnext-one.env
echo "BENCH_NETWORK=erpnext-one" >> ~/gitops/erpnext-one.env
echo '#' >> ~/gitops/erpnext-one.yaml
docker compose --project-name erpnext-one  --env-file ~/gitops/erpnext-one.env  -f compose.yaml  -f overrides/compose.erpnext.yaml  -f overrides/compose.redis.yaml -f docs/compose/compose.multi-bench.yaml -f docs/compose/compose.multi-bench-ssl.yaml config > ~/gitops/erpnext-one.yaml

docker compose --project-name erpnext-one -f ~/gitops/erpnext-one.yaml up -d


docker compose --project-name erpnext-one exec backend bench new-site erpdev.dcloud.co.id --mariadb-root-password changeit --install-app erpnext --admin-password D4t4c0mm2022Tende4n

docker-compose exec backend bench new-site erpdev.dcloud.co.id --mariadb-root-password changeit --admin-password D4t4c0mm2022Tende4n
