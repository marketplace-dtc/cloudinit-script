#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

MYSQL_ROOT_PASS='password'
MYSQL_USER_NAME='dbuser'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='db_default'
PORT='3306'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/phpmyadmin_mariadb_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=MYSQL_ROOT_PASS  
--user-name=MYSQL_USER_NAME 
--user-password=MYSQL_USER_PASS 
--user-database=MYSQL_USER_DB 
--port-database=PORT
--callbackurl=CALLBACKURL
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "phpmyadmin_mariadb_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--user-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-database=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--port-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--port-database=?*)
					PORT=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
echo "=> DB Username : $MYSQL_USER_NAME"
echo "=> DB Name : $MYSQL_USER_DB"

sudo apt-get update -y
sudo apt-get upgrade -y
curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash

# Make sure mariadb installs without prompts, and set the root password automatically.
export DEBIAN_FRONTEND=noninteractive
debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password password $MYSQL_ROOT_PASS"
debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password_again password $MYSQL_ROOT_PASS"

echo '=> Install mariadb.'
apt-get install -y mariadb-server

# Change the port and bind address.
echo "[mysqld]" >> /etc/mysql/mariadb.cnf
echo "port=$PORT" >> /etc/mysql/mariadb.cnf

echo '=> Restart services.'
sudo systemctl restart mariadb.service

#--------------------MARIADB CONF--------------------
echo '=> MariaDB database setup.'

# Clear the mysql.user table of all users except root.
echo "=> Clear mysql.user table (except root)."
mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "DELETE FROM mysql.user WHERE User NOT IN ('root', 'debian-sys-maint'); FLUSH PRIVILEGES;"

# Add the database if $MYSQL_USER_DB is set.
if [ ! -z "$MYSQL_USER_DB" ]; then
	echo "=> Creating $MYSQL_USER_DB."
	mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "CREATE DATABASE IF NOT EXISTS \`$MYSQL_USER_DB\`;"
fi

# Add the $MYSQL_USER_DB user if it exists and the user vars are set.
if [ ! -z "$MYSQL_USER_NAME" -a ! -z "$MYSQL_USER_PASS" -a ! -z "$MYSQL_USER_DB" ]; then
	echo "=> Creating $MYSQL_USER_NAME and granting privileges on $MYSQL_USER_DB."
	echo 'Creating user.'
	mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "CREATE USER '$MYSQL_USER_NAME'@'%' IDENTIFIED BY '$MYSQL_USER_PASS';"
	echo 'Created user.'
	echo 'Granting user.'
	mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON * . * TO '$MYSQL_USER_NAME'@'%' WITH GRANT OPTION;"
	echo 'Granted user.'
	mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "FLUSH PRIVILEGES;"
	echo 'Flush privileges.'
fi

echo '=> MariaDB database setup done'
sudo systemctl restart mariadb.service

sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install phpmyadmin

#!/bin/bash
echo "Include /etc/phpmyadmin/apache.conf"  >> /etc/apache2/apache2.conf

sudo systemctl restart apache2

curl --location --request GET "$CALLBACKURL"