#!/usr/bin/env bash
DOMAIN='example.com'
PASSWORD='Password123'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/harbor_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--domain=DOMAIN
--password=PASSWORD  
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "harbor_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--password=?*)
					PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

apt install apt-transport-https ca-certificates curl software-properties-common -y
sudo apt install certbot -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update -y
apt install docker-ce -y
curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
wget https://github.com/goharbor/harbor/releases/download/v2.4.1/harbor-offline-installer-v2.4.1.tgz
tar -xvzf harbor-offline-installer-v2.4.1.tgz
cd harbor
cp harbor.yml.tmpl harbor.yml

sed -e '/hostname:*/ s/^#*/#/' -i harbor.yml
sed -e '/harbor_admin_password:*/ s/^#*/#/' -i harbor.yml

sed -e '/port: 443/ s/^#*/#/' -i harbor.yml  
sed -e '/https:/ s/^#*/#/' -i harbor.yml  
sed -e '/certificate:*/ s/^#*/#/' -i harbor.yml  
sed -e '/private_key:*/ s/^#*/#/' -i harbor.yml
cat << EOF >> harbor.yml
hostname: $DOMAIN
EOF

cat << EOF >> harbor.yml
harbor_admin_password: $PASSWORD
EOF
./install.sh


curl --location --request GET "$CALLBACKURL"