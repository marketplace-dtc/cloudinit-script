#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
HOSTNAME='example'
CALLBACKURL='https://dcloud.co.id'

version="2022-03-14"
usage='USAGE : curl -LsS URL/docker_init.sh | bash -s -- [OPTIONS]
--hostname=hostname
--callbackurl=callbackurl
'   

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--hostname)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						HOSTNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--hostname=?*)
					HOSTNAME=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "docker_cloud_init_setup %s\n" "$version"
}

echo "apt update & upgrade"
sudo apt-get update -y
sudo apt-get upgrade -y

sudo hostnamectl set-hostname $DOMAIN

cat << EOF >> /etc/hosts
$HOSTNAME
EOF


echo "Install Docker CE"
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt-cache policy docker-ce
sudo apt-get update
sudo apt install docker-ce -y
sudo systemctl enable docker.service
sudo systemctl enable containerd.service


curl --location --request GET "$CALLBACKURL"