#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-07-07"

MONGO_USERNAME='default'
MONGO_PASSWORD='default'
PROJECT_NAME_EXPRESS='my-express'
PROJECT_NAME_ANGULAR='myAngular'
ANGULAR_PORT='4200'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/mean_init.sh | bash -s -- [OPTIONS]
Sample Options:
--mongo-username=MONGO_USERNAME
--mongo-password=MONGO_PASSWORD
--project-name-express=PROJECT_NAME_EXPRESS
--project-name-angular=PROJECT_NAME_ANGULAR 
--angular-port=ANGULAR_PORT
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "mean_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--project-name-express)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PROJECT_NAME_EXPRESS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--project-name-express=?*)
					PROJECT_NAME_EXPRESS=${1#*=}
					;;
		--project-name-angular)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PROJECT_NAME_ANGULAR=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--project-name-angular=?*)
					PROJECT_NAME_ANGULAR=${1#*=}
					;;
        --mongo-username)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MONGO_USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--mongo-username=?*)
					MONGO_USERNAME=${1#*=}
					;;
        --mongo-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MONGO_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--mongo-password=?*)
					MONGO_PASSWORD=${1#*=}
					;;
        --angular-port)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ANGULAR_PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--angular-port=?*)
					ANGULAR_PORT=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

sudo apt update
sudo apt dist-upgrade -y
	
sudo apt install gnupg
sudo apt update

#Update and install mongodb 
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

sudo apt update 
sudo apt install -y mongodb-org

#Starting mongodb service 
sudo systemctl enable mongod
sudo service mongod start
sudo service mongod status

#Config secure mongodb 
sudo sh -c 'echo "security:\n  authorization : enabled" >> /etc/mongod.conf'
set -e
sudo mongosh <<EOF
use admin
db.createUser(
  {
    user: "${MONGO_USERNAME}",
    pwd: "${MONGO_PASSWORD}",
    roles: [{ role: "readWrite", db: "admin" }]
  }
)

EOF
sudo service mongod restart

#Install nodejs latest 
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash -
sudo apt install nodejs -y
node --version
npm --version


#Install pm2 for deamon 
sudo npm i -g pm2 -y

#Install express js 
cd ~
sudo npm install -g express-generator@4
express ~/${PROJECT_NAME_EXPRESS}
cd ~/${PROJECT_NAME_EXPRESS}
sudo npm install
sudo pm2 start app.js

#Install angular 
cd ~ 
sudo npm install -g @angular/cli
yes yes | ng new $PROJECT_NAME_ANGULAR --defaults=true --skip-git --skip-tests
cd ~/$PROJECT_NAME_ANGULAR
sudo pm2 start "ng serve --host 0.0.0.0 --port $ANGULAR_PORT"

curl --location --request GET "$CALLBACKURL"