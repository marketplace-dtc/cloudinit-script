#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

CYBERPANEL_PASSWORD='DCloud123!'
CALLBACKURL = "https://dcloud.co.id"

usage='USAGE : curl -LsS URL/cyberpanel_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--cyberpanel-password=your-password
--callbackurl=http-webhook-notification
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Webmin_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--cyberpanel-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CYBERPANEL_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--cyberpanel-password=?*)
					CYBERPANEL_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

echo "apt update & upgrade"
apt -y update 
apt -y upgrade

echo "install Cyberpanel"
####Download and extract latest Cyberpanel Install Script
if test -f /root/install.sh
then
echo "install.sh is already downloaded."
else
echo "Downloading Cyberpanel"
cd /root/ && wget "https://cyberpanel.net/install.sh";
fi
echo "Create acme ln"
mkdir /.acme.sh/
ln -s /.acme.sh/ /root/.acme.sh
cd /root/
ls -al
echo "start instaling CyberPanel"
/bin/sh /root/install.sh --version ols --password $CYBERPANEL_PASSWORD

#finish install, send notification to webhook callbackurl
curl --location --request GET $CALLBACKURL






