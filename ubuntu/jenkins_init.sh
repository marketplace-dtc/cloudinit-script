#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
HOSTNAME='Jenkins'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/jenkins_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--hostname=hostname
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "jenkins_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--hostname)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						HOSTNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--hostname=?*)
					HOSTNAME=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update -y
sudo apt-get upgrade -y

sudo hostnamectl set-hostname $HOSTNAME

ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

cat << EOF >> /etc/hosts
$ip $HOSTNAME
EOF

sudo apt install openjdk-8-jre -y
sudo apt install default-jre -y
sudo apt install jenkins -y
sudo systemctl start jenkins
sudo ufw allow 8080

curl --location --request GET "$CALLBACKURL"

pass=$(cat /var/lib/jenkins/secrets/initialAdminPassword)
echo "PASWORDD JENKINS $pass"


