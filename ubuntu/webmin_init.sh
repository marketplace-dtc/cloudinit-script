#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

WEBMIN_USER='webmin'
WEBMIN_PASSWORD='webmin!@#'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/webmin_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--webmin-user=your-username
--webmin-password=your-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Webmin_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--webmin-user)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						WEBMIN_USER=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --webmin-user=?*)
					WEBMIN_USER=${1#*=}
					;;
		--webmin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						WEBMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--webmin-password=?*)
					WEBMIN_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

echo "apt update & upgrade"
sudo apt-get update -y
sudo apt-get upgrade -y

echo "add webmin user"
adduser --disabled-password --gecos "" $WEBMIN_USER
echo "$WEBMIN_USER:$WEBMIN_PASSWORD" |chpasswd
usermod -aG sudo $WEBMIN_USER

echo "install webmin"
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
wget -q -O- http://www.webmin.com/jcameron-key.asc | sudo apt-key add
apt -y update 
apt -y install webmin 

sed -i "s/ssl=1/ssl=0/g" /etc/webmin/miniserv.conf
service webmin restart

curl --location --request GET "$CALLBACKURL"