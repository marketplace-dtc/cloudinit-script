#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

ROOT_PASSWORD='cpanel!@#'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/cpanel_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=your-root-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Cpanel_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ROOT_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					ROOT_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

echo "apt update & upgrade"
apt -y update 
apt -y upgrade

echo "add root password"
echo "root:$ROOT_PASSWORD" |chpasswd

echo "install cpanel"
cd /home && curl -o latest -L https://securedownloads.cpanel.net/latest && sh latest
echo "finish instal cpanel"

echo "please go to https://YOURIP:2087 login with root and your root password to finish CPANEL setup"

curl --location --request GET "$CALLBACKURL"