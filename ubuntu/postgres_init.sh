#!/usr/bin/env bash

POSTGRESQL_USERNAME='user'
POSTGRESQL_DBNAME='userdb'
POSTGRESQL_PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/postgres_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--user-name=$user-name
--user-password=$user-password
--db-name=$db-name
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "postgres_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					POSTGRESQL_USERNAME=${1#*=}
					;;
		--db-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_DBNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-name=?*)
					POSTGRESQL_DBNAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					POSTGRESQL_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt-get update -y
sudo apt-get upgrade -y


# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql
sudo systemctl start postgresql.service
sudo ufw allow proto tcp from any to any port 5432
sudo -u postgres createuser $POSTGRESQL_USERNAME
sudo -u postgres psql -U postgres -d postgres -c "alter user $POSTGRESQL_USERNAME with password '$POSTGRESQL_PASSWORD';"
sudo -u postgres psql -U postgres -d postgres -c "ALTER USER $POSTGRESQL_USERNAME WITH SUPERUSER CREATEROLE CREATEDB REPLICATION;"
sudo -u postgres psql -d postgres -c "CREATE DATABASE $POSTGRESQL_DBNAME;"

echo '=> PostgreSql Installed'


curl --location --request GET "$CALLBACKURL"