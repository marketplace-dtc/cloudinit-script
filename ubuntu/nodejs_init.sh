#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

HOSTNAME='NodeJs'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/nodejs_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--hostname=HOSTNAME
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "nodejs_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--hostname)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						HOSTNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--hostname=?*)
					HOSTNAME=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

sudo apt-get update -y
sudo apt-get upgrade -y

ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

sudo hostnamectl set-hostname $HOSTNAME

cat << EOF >> /etc/hosts
$ip $HOSTNAME
EOF

curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs

curl --location --request GET "$CALLBACKURL"