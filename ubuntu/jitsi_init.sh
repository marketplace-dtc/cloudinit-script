#!/usr/bin/env bash
version="2021-07-30"

EMAIL='example@gmail.com'
DOMAIN='jitsi.example.com'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/jitsi_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--email=EMAIL 
--domain=DOMAIN
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "jitsi_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--email)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						EMAIL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--email=?*)
					EMAIL=${1#*=}
					;;
		--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

sudo apt-get update -y
sudo apt-get upgrade -y

#SET HOSTNAME
sudo hostnamectl set-hostname $DOMAIN

cat << EOF >> /etc/hosts
$ip $DOMAIN
EOF

#ALLOW HTTPS
sudo ufw allow OpenSSH
sudo ufw allow http
sudo ufw allow https
sudo ufw allow in 10000:20000/udp
echo "y" | sudo ufw enable



echo "jitsi-videobridge jitsi-videobridge/jvb-hostname string $DOMAIN" | debconf-set-selections
echo "jitsi-meet-web-config jitsi-meet/cert-choice select 'Generate a new self-signed certificate (You will later get a chance to obtain a Let's encrypt certificate)'" | debconf-set-selections

#DOWNLOAD REPOSITORY JITSI
wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
echo "deb https://download.jitsi.org stable/"  | sudo tee -a /etc/apt/sources.list.d/jitsi-stable.list

sudo apt update

#INSTALL JITSI
sudo apt install jitsi-meet -y

curl --location --request GET "$CALLBACKURL"