#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

MYSQL_ROOT_PASS='root'
MYSQL_USER_NAME='zabbix'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='zabbix_db'
ZABBIX_PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/zabbix_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=ROOTPASSWORD  
--user-name=USERNAME 
--user-password=USERPASSWORD 
--user-database=USERDB 
--zabbix-password=ZABBIXPASSWORD
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "zabbix_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--user-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-database=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--zabbix-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ZABBIX_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--zabbix-password=?*)
					ZABBIX_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

# Make sure mysql installs without prompts, and set the root password automatically.
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS"

echo '=> Install MySQL.'
sudo apt-get -y install mysql-server
sudo apt -y install apache2
sudo systemctl enable apache2

#--------------------MySQL CONF--------------------
echo '=> MySQL database setup.'

# Add the database if $MYSQL_USER_DB is set.
mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "create database $MYSQL_USER_DB character set utf8 collate utf8_bin;"

# Add the $MYSQL_USER_DB user if it exists and the user vars are set.
mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "CREATE USER '$MYSQL_USER_NAME'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASS';"
echo 'Created user.'

mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON * . * TO '$MYSQL_USER_NAME'@'localhost';"
echo 'Granted user.'

echo '=> MySQL database setup done'

sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install phpmyadmin

#!/bin/bash
echo "Include /etc/phpmyadmin/apache.conf"  >> /etc/apache2/apache2.conf
sudo systemctl restart apache2

sudo a2dissite 000-default.conf
sudo systemctl reload apache2
sudo systemctl restart apache2
wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb
sudo dpkg -i zabbix-release_5.0-1+focal_all.deb
apt-get update -y
apt-get install zabbix-server-mysql zabbix-frontend-php zabbix-agent zabbix-apache-conf -y

systemctl start zabbix-server
systemctl enable zabbix-server
cd /usr/share/doc/zabbix-server-mysql
zcat create.sql.gz | mysql -u $MYSQL_USER_NAME -p$MYSQL_USER_PASS $MYSQL_USER_DB

cat << EOF >> /etc/zabbix/zabbix_server.conf
DBHost=localhost
DBName=$MYSQL_USER_DB
DBUser=$MYSQL_USER_NAME
DBPassword=$MYSQL_USER_PASS

EOF
sed -i "s/^        # php_value/        php_value/g" /etc/zabbix/apache.conf
systemctl restart zabbix-server
systemctl restart apache2


cat << EOF >> /etc/zabbix/web/zabbix.conf.php
<?php
// Zabbix GUI configuration file.

\$DB['TYPE']                     = 'MYSQL';
\$DB['SERVER']                   = 'localhost';
\$DB['PORT']                     = '0';
\$DB['DATABASE']                 = '$MYSQL_USER_DB';
\$DB['USER']                     = '$MYSQL_USER_NAME';
\$DB['PASSWORD']                 = '$MYSQL_USER_PASS';

// Schema name. Used for PostgreSQL.
\$DB['SCHEMA']                   = '';

// Used for TLS connection.
\$DB['ENCRYPTION']               = false;
\$DB['KEY_FILE']                 = '';
\$DB['CERT_FILE']                = '';
\$DB['CA_FILE']                  = '';
\$DB['VERIFY_HOST']              = false;
\$DB['CIPHER_LIST']              = '';

// Use IEEE754 compatible value range for 64-bit Numeric (float) history values.
// This option is enabled by default for new Zabbix installations.
// For upgraded installations, please read database upgrade notes before enabling this option.
\$DB['DOUBLE_IEEE754']   = true;

\$ZBX_SERVER                    = 'localhost';
\$ZBX_SERVER_PORT               = '10051';
\$ZBX_SERVER_NAME               = '';

\$IMAGE_FORMAT_DEFAULT   = IMAGE_FORMAT_PNG;

// Uncomment this block only if you are using Elasticsearch.
// Elasticsearch url (can be string if same url is used for all types).
//\$HISTORY['url'] = [
//      'uint' => 'http://localhost:9200',
//      'text' => 'http://localhost:9200'
//];
// Value types stored in Elasticsearch.
//\$HISTORY['types'] = ['uint', 'text'];

// Used for SAML authentication.
// Uncomment to override the default paths to SP private key, SP and IdP X.509 certificates, and to set extra settings.
//\$SSO['SP_KEY']                        = 'conf/certs/sp.key';
//\$SSO['SP_CERT']                       = 'conf/certs/sp.crt';
//\$SSO['IDP_CERT']              = 'conf/certs/idp.crt';
//\$SSO['SETTINGS']              = [];
EOF

sudo mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "UPDATE $MYSQL_USER_DB.users set passwd=md5('$ZABBIX_PASSWORD') where alias='Admin';"

sudo systemctl restart apache2


curl --location --request GET "$CALLBACKURL"

