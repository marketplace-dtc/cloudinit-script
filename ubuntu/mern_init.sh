#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

PROJECT_NAME_REACT='my-react'
PROJECT_NAME_EXPRESS='my-express'
PORT_REACT='8082'
MONGO_USERNAME='default'
MONGO_PASSWORD='default'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/mern_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--project-name-react=$project-name-react
--project-name-express=$project-name-express
--port-react=$port-react
--mongo-username=$mongo-username
--mongo-password=$mongo-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "mern_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--project-name-react)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PROJECT_NAME_REACT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --project-name-react=?*)
					PROJECT_NAME_REACT=${1#*=}
					;;
        --project-name-express)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PROJECT_NAME_EXPRESS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --project-name-express=?*)
					PROJECT_NAME_EXPRESS=${1#*=}
					;;      
        --port-react)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT_REACT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --port-react=?*)
					PORT_REACT=${1#*=}
					;;  
        --mongo-username)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MONGO_USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --mongo-username=?*)
					MONGO_USERNAME=${1#*=}
					;;   
        --mongo-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MONGO_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --mongo-password=?*)
					MONGO_PASSWORD=${1#*=}
					;;  
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;       
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

sudo apt update -y
sudo apt dist-upgrade -y
	
sudo apt install gnupg
sudo apt update

# Download and add to source list 
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

#Update and install mongodb 
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt install -y mongodb-org

#Starting mongodb service 
sudo systemctl enable mongod
sudo service mongod start
sudo service mongod status

#Secure mongodb 
sudo sh -c 'echo "security:\n  authorization : enabled" >> /etc/mongod.conf'
set -e
sudo mongosh <<EOF
use admin
db.createUser(
  {
    user: "${MONGO_USERNAME}",
    pwd: "${MONGO_PASSWORD}",
    roles: [{ role: "readWrite", db: "admin" }]
  }
)

EOF

sudo service mongod restart

#Install nodejs 
curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs

#Install react js 
sudo npm install -g create-react-app
sudo create-react-app --version

#Install express js 
sudo npm install -g express-generator@4

#Install pm2 for deamon 
sudo npm i -g pm2 -y

#create express project 
express ~/${PROJECT_NAME_EXPRESS}
cd ~/${PROJECT_NAME_EXPRESS}
sudo npm install
sudo pm2 start app.js
cd ~
#create react project 
sudo npx create-react-app ${PROJECT_NAME_REACT} -y
cd ~/${PROJECT_NAME_REACT}
sudo npm run build
sudo pm2 serve build ${PORT_REACT} --spa


curl --location --request GET "$CALLBACKURL"
