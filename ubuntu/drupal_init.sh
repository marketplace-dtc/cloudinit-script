#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

MYSQL_ROOT_PASS='password123'
MYSQL_USER_NAME='user'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='dbdefault'
BIND='127.0.0.1'
PORT='3306'
DOMAIN='domain.example.com'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/drupal_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=ROOTPASSWORD  
--user-name=USERNAME 
--user-password=USERPASSWORD 
--user-database=USERDB 
--bind-address=IP 
--port-database=PORT
--domain=Domain
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "drupal_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--user-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-database=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--port-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--port-database=?*)
					PORT=${1#*=}
					;;
		--bind-address)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						BIND=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--bind-address=?*)
					BIND=${1#*=}
					;;
		--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

# Make sure mysql installs without prompts, and set the root password automatically.
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS"

echo '=> Install MySQL.'
sudo apt-get -y install mysql-server
sudo apt -y install apache2
sudo systemctl enable apache2

#--------------------MySQL CONF--------------------
echo '=> MySQL database setup.'

# Add the database if $MYSQL_USER_DB is set.
mysql -u root -p"$MYSQL_ROOT_PASS" -e "create database $MYSQL_USER_DB;"

# Add the $MYSQL_USER_DB user if it exists and the user vars are set.
mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "CREATE USER '$MYSQL_USER_NAME'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASS';"
echo 'Created user.'

mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON * . * TO '$MYSQL_USER_NAME'@'localhost';"
echo 'Granted user.'

echo '=> MySQL database setup done'

sudo apt install php7.4 libapache2-mod-php7.4 php7.4-{common,mbstring,xmlrpc,soap,gd,xml,intl,mysql,cli,zip,curl,fpm} -y
sudo wget https://www.drupal.org/download-latest/tar.gz -O drupal.tar.gz
sudo tar xvf drupal.tar.gz
sudo rm -f drupal*.tar.gz
sudo sudo mv drupal-*/  /var/www/html/drupal
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/

cat << EOF >> /etc/apache2/sites-available/drupal.conf
<VirtualHost *:80>
     ServerName $DOMAIN
     DocumentRoot /var/www/html/drupal/

     CustomLog ${APACHE_LOG_DIR}/access.log combined
     ErrorLog ${APACHE_LOG_DIR}/error.log

	<Directory /var/www/html/drupal>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
   </Directory>
</VirtualHost>

EOF

sudo a2ensite drupal.conf
sudo a2enmod rewrite
sudo systemctl restart apache2

sudo a2dissite 000-default.conf
sudo systemctl reload apache2
sudo systemctl restart apache2

curl --location --request GET "$CALLBACKURL"