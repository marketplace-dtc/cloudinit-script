#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

WEBMIN_USER='webmin'
WEBMIN_PASSWORD='webmin!@#'
WEBMIN_HOSTNAME='host.webmin'

usage='USAGE : curl -LsS URL/webmin_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--webmin-user=your-username
--webmin-password=your-password
--webmin-hostname=your-hostname-FQDN
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Webmin_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--webmin-user)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						WEBMIN_USER=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --webmin-user=?*)
					WEBMIN_USER=${1#*=}
					;;
		--webmin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						WEBMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--webmin-password=?*)
					WEBMIN_PASSWORD=${1#*=}
					;;
		--webmin-hostname)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						WEBMIN_HOSTNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--webmin-hostname=?*)
					WEBMIN_HOSTNAME=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

echo "apt update & upgrade"
apt -y update 
apt -y upgrade

echo "add webmin user"
adduser --disabled-password --gecos "" $WEBMIN_USER
echo "$WEBMIN_USER:$WEBMIN_PASSWORD" |chpasswd
usermod -aG sudo $WEBMIN_USER

echo "install Virtualmin and Webmin"
####Download and extract latest Virtualmin Install Script
if test -f /tmp/install.sh
then
echo "install.sh is already downloaded."
else
echo "Downloading Virtualmin"
cd /tmp/ && wget "https://software.virtualmin.com/gpl/scripts/install.sh";
fi

/bin/sh /tmp/install.sh -f -b LAMP -n $WEBMIN_HOSTNAME





