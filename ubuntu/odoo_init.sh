#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

ADMIN_PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/odoo_init.sh | bash -s -- [OPTIONS]
Sample Options:    
--admin-password=$admin-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "odoo_cloud_init_setup %s\n" "$version"
}
while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--admin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-password=?*)
					ADMIN_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt install git python3-pip build-essential wget python3-dev python3-venv \
    python3-wheel libfreetype6-dev libxml2-dev libzip-dev libldap2-dev libsasl2-dev \
    python3-setuptools node-less libjpeg-dev zlib1g-dev libpq-dev \
    libxslt1-dev libldap2-dev libtiff5-dev libjpeg8-dev libopenjp2-7-dev \
    liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev libxcb1-dev -y

sudo useradd -m -d /opt/odoo14 -U -r -s /bin/bash odoo14
sudo apt install postgresql -y
sudo su - postgres -c "createuser -s odoo14"

sudo wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb
sudo apt install ./wkhtmltox_0.12.5-1.bionic_amd64.deb -y


sudo -H -u odoo14 bash -c 'git clone https://www.github.com/odoo/odoo --depth 1 --branch 14.0 /opt/odoo14/odoo && cd /opt/odoo14 && python3 -m venv odoo-venv && source odoo-venv/bin/activate && pip3 install wheel && pip3 install -r odoo/requirements.txt && deactivate'

mkdir /opt/odoo14/odoo-custom-addons

cat << EOF >> /etc/odoo14.conf
[options]
; This is the password that allows database operations:
admin_passwd = $ADMIN_PASSWORD
db_host = False
db_port = False
db_user = odoo14
db_password = False
addons_path = /opt/odoo14/odoo/addons,/opt/odoo14/odoo-custom-addons

EOF


cat << EOF >> /etc/systemd/system/odoo14.service
[Unit]
Description=Odoo14
Requires=postgresql.service
After=network.target postgresql.service

[Service]
Type=simple
SyslogIdentifier=odoo14
PermissionsStartOnly=true
User=odoo14
Group=odoo14
ExecStart=/opt/odoo14/odoo-venv/bin/python3 /opt/odoo14/odoo/odoo-bin -c /etc/odoo14.conf
StandardOutput=journal+console

[Install]
WantedBy=multi-user.target

EOF


sudo systemctl daemon-reload
sudo systemctl enable --now odoo14


curl --location --request GET "$CALLBACKURL"
