#!/usr/bin/env bash

POSTGRESQL_USERNAME='wikijs'
POSTGRESQL_PASSWORD='wikijs'
POSTGRESQL_DATABASE='wikidb'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/wikijs_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--user-name=$user-name
--user-password=$user-password
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "wikijs_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					POSTGRESQL_USERNAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					POSTGRESQL_PASSWORD=${1#*=}
					;;
		--database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						POSTGRESQL_DATABASE=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--database=?*)
					POSTGRESQL_DATABASE=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;            
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs



# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt-get update -y
sudo apt-get upgrade -y

# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql
sudo systemctl start postgresql.service
sudo ufw allow proto tcp from any to any port 5432
sudo -u postgres createuser $POSTGRESQL_USERNAME
sudo -u postgres psql -U postgres -d postgres -c "alter user $POSTGRESQL_USERNAME with password '$POSTGRESQL_PASSWORD';"
sudo -u postgres psql -U postgres -d postgres -c "ALTER USER $POSTGRESQL_USERNAME WITH SUPERUSER CREATEROLE CREATEDB REPLICATION;"

sudo -u postgres psql -U postgres -d postgres -c "create database $POSTGRESQL_DATABASE;"
#install wikijs


sudo apt -y install nginx
sudo apt -y install letsencrypt
sudo apt -y install python3-certbot-nginx

#clear nginx default conf
echo "" > /etc/nginx/sites-enabled/default
cat << \EOF >> /etc/nginx/sites-enabled/default
# BEGIN Default Conf
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name _;

        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://127.0.0.1:3000;
        }
}
# END Default Conf
EOF
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl restart nginx


sudo mkdir -p /var/www/wikijs
cd /var/www/wikijs
wget https://github.com/Requarks/wiki/releases/latest/download/wiki-js.tar.gz
tar xzf wiki-js.tar.gz
cp config.sample.yml config.yml

sed -i 's/user: wikijs/user: '$POSTGRESQL_USERNAME'/g' config.yml
sed -i 's/pass: wikijsrocks/pass: '$POSTGRESQL_PASSWORD'/g' config.yml
sed -i 's/db: wiki/db: '$POSTGRESQL_DATABASE'/g' config.yml
nohup node server &


curl --location --request GET "$CALLBACKURL"