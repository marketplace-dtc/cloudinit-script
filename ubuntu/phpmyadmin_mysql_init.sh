#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

MYSQL_ROOT_PASS='password123'
MYSQL_USER_NAME='user'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='dbdefault'
BIND='127.0.0.1'
PORT='3306'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/phpmyadmin_mysql_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=ROOTPASSWORD  
--user-name=USERNAME 
--user-password=USERPASSWORD 
--user-database=USERDB 
--bind-address=IP 
--port-database=PORT
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "phpmyadmin_mysql_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--user-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-database=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--port-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--port-database=?*)
					PORT=${1#*=}
					;;
		--bind-address)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						BIND=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--bind-address=?*)
					BIND=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

# Make sure mysql installs without prompts, and set the root password automatically.
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS"

echo '=> Install MySQL.'
sudo apt-get -y install mysql-server
sudo apt -y install apache2
sudo systemctl enable apache2

# # Change the port and bind address.

echo "[mysqld]" >> /etc/mysql/my.cnf
echo "port=$PORT" >> /etc/mysql/my.cnf
echo "bind-address=$BIND" >> /etc/mysql/my.cnf

#--------------------MySQL CONF--------------------
echo '=> MySQL database setup.'

# Add the database if $MYSQL_USER_DB is set.
mysql -u root -p"$MYSQL_ROOT_PASS" -e "create database $MYSQL_USER_DB;"

# Add the $MYSQL_USER_DB user if it exists and the user vars are set.
mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "CREATE USER '$MYSQL_USER_NAME'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASS';"
echo 'Created user.'

mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON * . * TO '$MYSQL_USER_NAME'@'localhost';"
echo 'Granted user.'

mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "FLUSH PRIVILEGES;"
echo 'Flush privileges.'

echo '=> MySQL database setup done'

sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install phpmyadmin

#!/bin/bash
echo "Include /etc/phpmyadmin/apache.conf"  >> /etc/apache2/apache2.conf
sudo systemctl restart apache2

sudo a2dissite 000-default.conf
sudo systemctl reload apache2
sudo systemctl restart mysql
sudo systemctl restart apache2


curl --location --request GET "$CALLBACKURL"