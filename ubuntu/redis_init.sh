#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

PORT='6379'
AUTH='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/redis_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--port=port
--auth=auth
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "redis_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--port)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --port=?*)
					PORT=${1#*=}
					;;
		--auth)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						AUTH=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --auth=?*)
					AUTH=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt -y install redis-server
sudo systemctl enable --now redis-server


cat << EOF >> /etc/redis/redis.conf
supervised systemd
requirepass $AUTH
EOF

sed -i "s/port 6379/port $PORT/g" /etc/redis/redis.conf


sudo systemctl restart redis.service
sudo ufw allow $PORT/tcp
sudo ufw reload
sudo systemctl restart redis.service


curl --location --request GET "$CALLBACKURL"
