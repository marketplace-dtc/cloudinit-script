#!/usr/bin/env bash

USERNAME='user'
PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/rabbit_mq_init.sh | bash -s -- [OPTIONS]
Sample Options:    
--user-name=$user-name
--user-password=$user-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "rabbit_mq_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --user-name=?*)
					USERNAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done



sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install apt-transport-https curl gnupg -y
curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list <<EOF
deb https://dl.bintray.com/rabbitmq-erlang/debian $(lsb_release -cs) erlang
deb https://dl.bintray.com/rabbitmq/debian $(lsb_release -cs) main
EOF

echo "=================================================================="
echo "Rabbit MQ Installing . . . . "
echo "=================================================================="


sudo apt-get update -y
sudo apt-get install rabbitmq-server -y --fix-missing

sudo systemctl is-enabled rabbitmq-server.service

sudo ufw allow proto tcp from any to any port 5672,15672


curl --location --request GET "$CALLBACKURL"

sudo rabbitmq-plugins enable rabbitmq_management && sudo rabbitmqctl add_user "$USERNAME" "$PASSWORD" && sudo rabbitmqctl set_user_tags "$USERNAME" administrator && sudo rabbitmqctl set_permissions -p / "$USERNAME" ".*" ".*" ".*"


