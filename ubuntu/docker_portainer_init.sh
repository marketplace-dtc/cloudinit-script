#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

ADMIN_USER='admin'
ADMIN_PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/docker_portainer_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--admin-user=some-user
--admin-password=your-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "Docker_portainer_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--admin-user)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_USER=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --admin-user=?*)
					ADMIN_USER=${1#*=}
					;;
		--admin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-password=?*)
					ADMIN_PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

echo "apt update & upgrade"
sudo apt -y update 
sudo apt -y upgrade

echo "Install Docker CE"
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt -y update 
sudo apt -y install docker-ce
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
echo "Install Portainer"

/usr/bin/docker run -d -p 8000:8000 -p 9443:9443 --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:2.11.1

echo "Delay 30 sec"
sleep 30
echo "Init Admin Password"

curl -k 'https://localhost:9443/api/users/admin/init' \
  -H 'authority: localhost:9443' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"' \
  -H 'accept: application/json, text/plain, */*' \
  -H 'content-type: application/json' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  -H 'origin: https://localhost:9443' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://localhost:9443/' \
  -H 'accept-language: en-US,en;q=0.9,id;q=0.8,ms;q=0.7' \
  --data-raw '{"Username":"'"$ADMIN_USER"'" ,"Password":"'"$ADMIN_PASSWORD"'"}' \
  --compressed \
  --insecure

  echo "Cloud init Done"
  

curl --location --request GET "$CALLBACKURL"