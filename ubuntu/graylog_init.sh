#!/usr/bin/env bash
version="2021-07-30"
PASSWORD='password'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/graylog_init.sh | bash -s -- [OPTIONS]
Sample Options:    
--password=$password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "graylog_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --password=?*)
					PASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done



sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt -y install bash-completion apt-transport-https uuid-runtime pwgen openjdk-11-jre-headless
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/oss-6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list

sudo apt update
sudo apt -y install elasticsearch-oss

cat << EOF >> /etc/elasticsearch/elasticsearch.yml
cluster.name: graylog
action.auto_create_index: false
EOF


sleep 5
sudo systemctl daemon-reload
sudo systemctl restart elasticsearch
sudo systemctl enable elasticsearch

sudo apt install mongodb-server -y
sudo systemctl start mongodb
sudo systemctl enable mongodb

wget https://packages.graylog2.org/repo/packages/graylog-4.1-repository_latest.deb
sudo dpkg -i graylog-4.1-repository_latest.deb

sudo apt update
sudo apt -y install graylog-server

sudo apt install pwgen -y

pw=$(pwgen -N 1 -s 96)

sed -i "s/password_secret =/password_secret = $pw/g" /etc/graylog/server/server.conf

generate=$(printf '%s' "$PASSWORD" | sha256sum | cut -f1 -d' ')

sed -i "s/root_password_sha2 =/root_password_sha2 = $generate/g" /etc/graylog/server/server.conf

cat << EOF >> /etc/graylog/server/server.conf
http_bind_address = 0.0.0.0:9000
EOF

sudo systemctl daemon-reload
sudo systemctl restart graylog-server
sudo systemctl enable graylog-server


curl --location --request GET "$CALLBACKURL"

