#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

USERPASSWORD='Password123'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/grafana_init.sh | bash -s -- [OPTIONS]
Sample Options:    
--user-password=$user-password
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "grafana_cloud_init_setup %s\n" "$version"
}
while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						USERPASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					USERPASSWORD=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
IP_ADDRESS=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo apt-get install -y apt-transport-https
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt install grafana -y
sudo apt -y install nginx
sudo apt -y install letsencrypt
sudo apt -y install python3-certbot-nginx

#clear nginx default conf
echo "" > /etc/nginx/sites-enabled/default
cat << EOF >> /etc/nginx/sites-enabled/default
# BEGIN Odoo Default Conf
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name _;

        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://127.0.0.1:3000;
        }
}
# END Odoo Default Conf
EOF

sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl enable grafana-server.service
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl restart nginx

sudo grafana-cli admin reset-admin-password $USERPASSWORD

curl --location --request GET "$CALLBACKURL"