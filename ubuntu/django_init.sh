#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

DJANGO_USERNAME='admin'
DJANGO_PASSWORD='password123'
DJANGO_EMAIL='admin@example.com'
DJANGO_PROJECT='project'
PORT='8000'
ENV='env'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/django_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--user-name=your-username  
--user-password=your-password  
--user-email=your-email  
--user-project=your-projectname 
--env=your-env
--port=ypur-port
--callbackurl=callbackurl
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "django_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DJANGO_USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					DJANGO_USERNAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DJANGO_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					DJANGO_PASSWORD=${1#*=}
					;;
		--user-email)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DJANGO_EMAIL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-email=?*)
					DJANGO_EMAIL=${1#*=}
					;;
		--user-project)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DJANGO_PROJECT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-project=?*)
					DJANGO_PROJECT=${1#*=}
					;;
		--port)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--port=?*)
					PORT=${1#*=}
					;;
		--env)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ENV=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--env=?*)
					ENV=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done


sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt -y install apache2
sudo systemctl enable apache2

sudo apt install -y python3-pip
sudo apt install -y build-essential libssl-dev libffi-dev python3-dev
sudo apt install -y python3-venv
sudo apt install python3-virtualenv -y

#!/bin/bash

mkdir django
cd django
virtualenv $ENV
cd $ENV
. bin/activate

pip install django
sudo ufw allow 8000

django-admin startproject $DJANGO_PROJECT 

sed -i "s/ALLOWED_HOSTS = \[]/ALLOWED_HOSTS = ['*']/g" $DJANGO_PROJECT/$DJANGO_PROJECT/settings.py

cd $DJANGO_PROJECT
python3 manage.py migrate
export DJANGO_SUPERUSER_USERNAME=$DJANGO_USERNAME
export DJANGO_SUPERUSER_EMAIL=$DJANGO_EMAIL
export DJANGO_SUPERUSER_PASSWORD=$DJANGO_PASSWORD
python3 manage.py createsuperuser --no-input
nohup python3 manage.py runserver 0.0.0.0:$PORT &


curl --location --request GET "$CALLBACKURL"
