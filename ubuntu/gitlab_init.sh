#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

DOMAIN='gitlab.example.com'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/gitlab_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--domain=DOMAIN
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "gitlab_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

sudo apt update -y
sudo apt-get upgrade -y

sudo hostnamectl set-hostname $DOMAIN

cat << EOF >> /etc/hosts
$ip $DOMAIN
EOF

sudo apt-get install -y curl openssh-server ca-certificates
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
sudo apt-get install gitlab-ce -y
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/bionic/gitlab-ce_13.2.1-ce.0_amd64.deb/download.deb
sudo dpkg -i gitlab-ce_13.2.1-ce.0_amd64.deb
sudo gitlab-ctl reconfigure
sudo gitlab-ctl start


curl --location --request GET "$CALLBACKURL"