#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2022-03-14"

MYSQL_ROOT_PASS='password'
MYSQL_USER_NAME='dbuser'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='db_default'
ADMIN_USER='admin'
ADMIN_PASSWORD='password!@#'
ADMIN_EMAIL="admin@example.com"
BLOG_TITLE="Wordpress Title"
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/wordpress_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--db-root-password=ROOTPASSWORD  
--db-user-name=USERNAME 
--db-user-password=USERPASSWORD 
--db-database-name=USERDB 
--admin-user=useradmin 
--admin-password=password 
--admin-email=admin@email.com 
--blog-title=Wordpress title
--callbackurl=callbackurl 
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "wordpress_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--db-root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--db-user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--db-user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--db-database-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--db-database-name=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--admin-user)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_USER=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-user=?*)
					ADMIN_USER=${1#*=}
					;;
		--admin-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_PASSWORD=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-password=?*)
					ADMIN_PASSWORD=${1#*=}
					;;
		--admin-email)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						ADMIN_EMAIL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--admin-email=?*)
					ADMIN_EMAIL=${1#*=}
					;;
		--blog-title)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						BLOG_TITLE=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--blog-title=?*)
					BLOG_TITLE=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;

		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

install_dir="/var/www/html"
#Creating WP Database Credenitals
db_name=$MYSQL_USER_DB
db_user=$MYSQL_USER_NAME
db_password=$MYSQL_USER_PASS
mysqlrootpass=$MYSQL_ROOT_PASS
web_blog_title=$BLOG_TITLE
admin_username=$ADMIN_USER
admin_password=$ADMIN_PASSWORD
admin_email=$ADMIN_EMAIL

#### Install Packages for https and mysql
apt -y update 
apt -y upgrade
apt -y install lynx
apt -y install apache2
apt -y install mysql-server


#### Start http
rm /var/www/html/index.html
systemctl enable apache2
systemctl start apache2

#### Start mysql and set root password

systemctl enable mysql
systemctl start mysql

/usr/bin/mysql -e "USE mysql;"
/usr/bin/mysql -e "UPDATE user SET Password=PASSWORD($mysqlrootpass) WHERE user='root';"
/usr/bin/mysql -e "FLUSH PRIVILEGES;"
touch /root/.my.cnf
chmod 640 /root/.my.cnf
echo "[client]">>/root/.my.cnf
echo "user=root">>/root/.my.cnf
echo "password="$mysqlrootpass>>/root/.my.cnf
####Install PHP
apt -y install php php-bz2 php-mysqli php-curl php-gd php-intl php-common php-mbstring php-xml

sed -i '0,/AllowOverride\ None/! {0,/AllowOverride\ None/ s/AllowOverride\ None/AllowOverride\ All/}' /etc/apache2/apache2.conf #Allow htaccess usage

systemctl restart apache2

####Download and extract latest WordPress Package
if test -f /tmp/latest.tar.gz
then
echo "WP is already downloaded."
else
echo "Downloading WordPress"
cd /tmp/ && wget "http://wordpress.org/latest.tar.gz";
fi

/bin/tar -C $install_dir -zxf /tmp/latest.tar.gz --strip-components=1
chown www-data: $install_dir -R

#### Create WP-config and set DB credentials
/bin/mv $install_dir/wp-config-sample.php $install_dir/wp-config.php

/bin/sed -i "s/database_name_here/$db_name/g" $install_dir/wp-config.php
/bin/sed -i "s/username_here/$db_user/g" $install_dir/wp-config.php
/bin/sed -i "s/password_here/$db_password/g" $install_dir/wp-config.php

cat << EOF >> $install_dir/wp-config.php
define('FS_METHOD', 'direct');
EOF

cat << EOF >> $install_dir/.htaccess
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index.php$ – [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
# END WordPress
EOF

chown www-data: $install_dir -R

##### Set WP Salts
grep -A50 'table_prefix' $install_dir/wp-config.php > /tmp/wp-tmp-config
/bin/sed -i '/**#@/,/$p/d' $install_dir/wp-config.php
/usr/bin/lynx --dump -width 200 https://api.wordpress.org/secret-key/1.1/salt/ >> $install_dir/wp-config.php
/bin/cat /tmp/wp-tmp-config >> $install_dir/wp-config.php && rm /tmp/wp-tmp-config -f
/usr/bin/mysql -u root -e "CREATE DATABASE $db_name"
/usr/bin/mysql -u root -e "CREATE USER '$db_user'@'localhost' IDENTIFIED WITH mysql_native_password BY '$db_password';"
/usr/bin/mysql -u root -e "GRANT ALL PRIVILEGES ON $db_name.* TO '$db_user'@'localhost';"
 
######Wordpress Init.
sleep 1m
curl --location --request POST 'http://localhost/wp-admin/install.php?step=2' \
--header 'Connection: keep-alive' \
--header 'Cache-Control: max-age=0' \
--header 'Upgrade-Insecure-Requests: 1' \
--header 'Origin: http://localhost' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
--header 'Referer: http://localhost/wp-admin/install.php?step=1' \
--header 'Accept-Language: en-US,en;q=0.9,id;q=0.8,ms;q=0.7' \
--data-urlencode weblog_title="$web_blog_title" \
--data-urlencode 'user_name='$admin_username \
--data-urlencode 'admin_password='$admin_password \
--data-urlencode 'admin_password2='$admin_password \
--data-urlencode 'admin_email='$admin_email \
--data-urlencode 'Submit=Install WordPress' \
--data-urlencode 'language='

sed -i "2 i ##REMOVE THIS AFTER YOU FINISH SETUP YOUR WORDPRESS SITE AND HOME URL ADDRES## " $install_dir/wp-config.php
sed -i "3 i define( 'WP_SITEURL','http://'.\$_SERVER['HTTP_HOST'].'/' ); " $install_dir/wp-config.php
sed -i "4 i define( 'WP_HOME','http://'.\$_SERVER['HTTP_HOST'].'/' ); " $install_dir/wp-config.php
sed -i "5 i ##END OF REMOVE ### " $install_dir/wp-config.php

######Display generated passwords to log file.
echo "Database Name: " $db_name
echo "Database User: " $db_user
echo "Database Password: " $db_password
echo "Mysql root password: " $mysqlrootpass
echo "admin user: " $admin_username
echo "admin password: " $admin_password
echo "blog title: " $web_blog_title

echo '=> Wordpress setup done'


curl --location --request GET "$CALLBACKURL"