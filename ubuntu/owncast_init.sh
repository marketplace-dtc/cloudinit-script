#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

DOMAIN='domain.example.com'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/owncast_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--domain=DOMAIN
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "owncast_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
					--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done

ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

sudo apt-get update -y
sudo apt-get upgrade -y

sudo hostnamectl set-hostname $DOMAIN

cat << EOF >> /etc/hosts
$ip $DOMAIN
EOF

echo 'Starting download  . . .'
sudo apt install unzip
wget https://owncast.online/install.sh
sudo bash install.sh 
cd owncast
sudo nohup ./owncast &>/dev/null &
sudo ufw allow 8080/tcp
sudo ufw allow 1935/tcp
sudo ufw reload


curl --location --request GET "$CALLBACKURL"