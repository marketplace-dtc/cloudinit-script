#!/usr/bin/env bash
version="2022-03-14"
USERNAME="user"

usage='USAGE : curl -LsS URL/prometheus_init.sh | bash -s -- [OPTIONS]
Sample Options:    
--user-name=$user-name
'
msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "prometheus_init_setup %s\n" "$version"
}
while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						USERNAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					USERNAME=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
sudo apt-get update
sudo apt-get upgrade -y
sudo mkdir -p /etc/prometheus
sudo mkdir -p /var/lib/prometheus
wget https://github.com/prometheus/prometheus/releases/download/v2.31.0/prometheus-2.31.0.linux-amd64.tar.gz 
tar -xvf prometheus-2.31.0.linux-amd64.tar.gz
cd prometheus-2.31.0.linux-amd64
sudo mv prometheus promtool /usr/local/bin/
sudo mv consoles/ console_libraries/ /etc/prometheus/
sudo mv prometheus.yml /etc/prometheus/prometheus.yml

sudo groupadd --system prometheus
sudo useradd -s /sbin/login --system -g prometheus $USERNAME
sudo chown -R $USERNAME:prometheus /etc/prometheus/ /var/lib/prometheus/
sudo chmod -R 775 /etc/prometheus/ /var/lib/prometheus/

cat << EOF >> /etc/systemd/system/prometheus.service
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=$USERNAME
Group=prometheus
Restart=always
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file=/etc/prometheus/prometheus.yml \
    --storage.tsdb.path=/var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries \
    --web.listen-address=0.0.0.0:9090

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl start prometheus
sudo systemctl enable prometheus
sudo ufw allow 9090/tcp
sudo ufw reload

NEWVARID=$(cat /etc/environment | grep VARID)

VARID2=$(sed -e 's#.*=\(\)#\1#' <<< "$NEWVARID")

curl --location --request GET "https://sanggar.cloudciti.io/openstack/api/server/'$VARID2'/validate"



sleep 5
sed -i "s/VARID=$VARID2/VARID=''/g" /etc/environment