#!/usr/bin/env bash
# shellcheck disable=2016 disable=1091 disable=2059
version="2021-07-30"

MYSQL_ROOT_PASS='password'
MYSQL_USER_NAME='akaunting_user'
MYSQL_USER_PASS='password'
MYSQL_USER_DB='akaunting'
BIND='127.0.0.1'
PORT='3306'
DOMAIN='akaunting.example.com'
CALLBACKURL='https://dcloud.co.id'

usage='USAGE : curl -LsS URL/akaunting_init.sh | bash -s -- [OPTIONS]
Sample Options: 
--root-password=ROOTPASSWORD  
--user-name=USERNAME 
--user-password=USERPASSWORD 
--user-database=USERDB 
--bind-address=IP 
--port-database=PORT
--domain=DOMAIN
--callbackurl=callbackurl
'

msg(){
    type=$1 #${1^^}
    shift
    printf "[$type] %s\n" "$@" >&2
}

error(){
    msg error "$@"
    exit 1
}

version(){
    printf "akaunting_cloud_init_setup %s\n" "$version"
}

while :; do
    case $1 in
		--version)
					version
					exit 0
					;;
		--root-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_ROOT_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--root-password=?*)
					MYSQL_ROOT_PASS=${1#*=}
					;;
		--user-name)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_NAME=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-name=?*)
					MYSQL_USER_NAME=${1#*=}
					;;
		--user-password)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_PASS=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-password=?*)
					MYSQL_USER_PASS=${1#*=}
					;;
		--user-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						MYSQL_USER_DB=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--user-database=?*)
					MYSQL_USER_DB=${1#*=}
					;;
		--port-database)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						PORT=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--port-database=?*)
					PORT=${1#*=}
					;;
		--bind-address)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						BIND=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						DOMAIN=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
		--domain=?*)
					DOMAIN=${1#*=}
					;;
		--callbackurl)
					if [[ -n $2 ]] && [[ $2 != --* ]]; then
						CALLBACKURL=$2
						shift
					else
						error "The $1 option requires an argument"
					fi
					;;
        --callbackurl=?*)
					CALLBACKURL=${1#*=}
					;;
		--help)
					version
					printf "%s" "$usage"
					exit
					;;
        -?*)
					msg warning "Unknown option (ignored): $1\n"
					;;
		*)
            break
    esac
    shift
done
echo "=> DB Username : $MYSQL_USER_NAME"
echo "=> DB Name : $MYSQL_USER_DB"

sudo apt-get update -y
sudo apt-get upgrade -y

curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash

# Make sure mariadb installs without prompts, and set the root password automatically.
export DEBIAN_FRONTEND=noninteractive
debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password password $MYSQL_ROOT_PASS"
debconf-set-selections <<< "mariadb-server-10.0 mysql-server/root_password_again password $MYSQL_ROOT_PASS"
#mysql -uroot -pPASS -e "SET PASSWORD = PASSWORD('');"

echo '=> Install mariadb.'
sudo apt-get install -y mariadb-server

# Change the port and bind address.
echo "port=$PORT" >> /etc/mysql/my.cnf
echo "[mysqld]" >> /etc/mysql/my.cnf
echo "bind-address=$BIND" >> /etc/mysql/my.cnf

echo '=> Restart services.'
sudo service mysql restart

#--------------------MARIADB CONF--------------------
echo '=> MariaDB database setup.'

# Clear the mysql.user table of all users except root.
echo "=> Clear mysql.user table (except root)."
sudo mysql -u 'root' -p"$MYSQL_ROOT_PASS" -e "DELETE FROM mysql.user WHERE User NOT IN ('root', 'debian-sys-maint'); FLUSH PRIVILEGES;"
#mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "DROP USER IF EXISTS (SELECT GROUP_CONCAT(User) FROM mysql.user WHERE User NOT IN ('root', 'debian-sys-maint') GROUP BY User);"

# Add the database if $MYSQL_USER_DB is set.
if [ ! -z "$MYSQL_USER_DB" ]; then
	echo "=> Creating $MYSQL_USER_DB."
	mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "CREATE DATABASE IF NOT EXISTS \`$MYSQL_USER_DB\`;"
fi

# Add the $MYSQL_USER_DB user if it exists and the user vars are set.
if [ ! -z "$MYSQL_USER_NAME" -a ! -z "$MYSQL_USER_PASS" -a ! -z "$MYSQL_USER_DB" ]; then
	echo "=> Creating $MYSQL_USER_NAME and granting privileges on $MYSQL_USER_DB."
	echo 'Creating user.'
	mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "CREATE USER '$MYSQL_USER_NAME'@'%' IDENTIFIED BY '$MYSQL_USER_PASS';"
	echo 'Created user.'
	echo 'Granting user.'
	mysql -u'root' -p"$MYSQL_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON \`$MYSQL_USER_DB\`.* TO '$MYSQL_USER_NAME'@'%' WITH GRANT OPTION;"
	echo 'Granted user.'
fi

echo '=> MariaDB database setup done'
wget -O Akaunting.zip https://akaunting.com/download.php?version=latest
sudo mkdir -p /var/www/akaunting/
sudo apt install unzip
sudo unzip Akaunting.zip -d /var/www/akaunting/
sudo chown www-data:www-data -R /var/www/akaunting/
sudo chmod -R 755 /var/www/akaunting/
sudo apt install software-properties-common

sudo add-apt-repository ppa:ondrej/php -y
apt install php8.0 -y
apt install libapache2-mod-php8.0 -y
apt install php8.0-mysql -y 
apt install php8.0-gd -y
apt install php8.0-cli -y
apt install php8.0-imagick -y
apt install php8.0-mysql -y
apt install php8.0-bcmath -y
apt install php8.0-curl -y
apt install php8.0-phpdbg -y
apt install php8.0-soap -y
apt install php8.0-mbstring -y
apt install php8.0-zip -y
apt install php8.0-xml -y
apt install php8.0-intl -y

sudo apt -y install apache2
sudo systemctl enable apache2
cat << EOF >> /etc/apache2/sites-available/akaunting.conf
<VirtualHost *:80>
    ServerName $DOMAIN
    DocumentRoot /var/www/akaunting/

    <Directory /var/www/akaunting/>
       DirectoryIndex index.php
       Options +FollowSymLinks
       AllowOverride All
       Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/akaunting.error.log
    CustomLog ${APACHE_LOG_DIR}/akaunting.access.log combined

</VirtualHost>

EOF

sudo a2ensite akaunting.conf
sudo a2enmod rewrite

sudo a2dissite 000-default.conf
sudo systemctl reload apache2
sudo systemctl restart apache2


curl --location --request GET "$CALLBACKURL"

